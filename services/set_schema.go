package services

import (
	"context"
	"net/http"

	"github.com/dgraph-io/dgo/v200/protos/api"
	"github.com/gin-gonic/gin"
)

func DropAll(c *gin.Context) {
	err := client.Alter(context.Background(), &api.Operation{DropOp: api.Operation_ALL})
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"message": "drop all error: " + err.Error(),
			"code":    2,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "drop all success",
	})

}
