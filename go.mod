module e_world_admin_backend

go 1.14

require (
	github.com/buger/jsonparser v1.0.0
	github.com/dgraph-io/dgo v1.0.0
	github.com/dgraph-io/dgo/v200 v200.0.0-20201023081658-a9ad93fe6ebd
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	google.golang.org/grpc v1.23.0
)
