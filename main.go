package main

import (
	"e_world_admin_backend/route"
	"log"
)

func main() {
	route.AuthRoute()
	route.SchemaOperation()
	err := route.Router.Run(":8999")
	if err != nil {
		log.Println("run server error: " + err.Error())
	}
}
