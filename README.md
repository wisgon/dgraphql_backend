# e_world_admin_backend

#### 介绍

go 语言写的后台辅助，就是负责返回 jwt 用的，也完全可以用其他语言写，反正能返回 jwt 就可以了；

这个后台是用来 login 用的，也可能有些业务逻辑必须用后端来解决，那么就可以用后端开一个路由来做，然后用 nginx 来反向代理这个接口路由即可；

#### 与本项目匹配的前端 dgraph 项目：

[前端及部署方法](https://gitee.com/wisgon/dgraph_graphql_module)
